# Recipe App aPI proxy
NGINX app proxy image for recipie app API

# Usage

## Environment variables.
 
 * `LISTEN_PORT` - Port to listen on defaults to `8000`
 * `APP_HOST` - Host name of the app forward the requests to `app`
 * `APP_PORT` - Port of the app to forward requests to  defaults to `9000`